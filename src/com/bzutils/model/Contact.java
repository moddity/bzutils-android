package com.bzutils.model;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * Created by iaguila on 19/07/13.
 */
public class Contact {

    private String name;
    private String id;
    private String urlFoto;
    private ArrayList<String> emails;
    private Drawable foto;
    private String phone;

    @Override
    public boolean equals(Object obj){
        if (((Contact) obj).getName().equals(this.getName()))
            return true;
        return false;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public ArrayList<String> getEmails() {
        return emails;
    }

    public void setEmails(ArrayList<String> emails) {
        this.emails = emails;
    }

    public Drawable getFoto() {
        return foto;
    }

    public void setFoto(Drawable foto) {
        this.foto = foto;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

package com.bzutils.time;

import java.util.Date;

/**
 * Created by iaguila on 25/5/15.
 */
public class BZTimeUtils {

    public static long getUnixTimestamp() {
        return new Date().getTime() / 1000;
    }
}

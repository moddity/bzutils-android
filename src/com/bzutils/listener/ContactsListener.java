package com.bzutils.listener;

import com.bzutils.model.Contact;

import java.util.ArrayList;
import java.util.EventListener;

/**
 * Created by iaguila on 19/07/13.
 */
public interface ContactsListener extends EventListener {

    public void onContactsReceived(ArrayList<Contact> contacts);

    public void onContactsFailed(String error);
}

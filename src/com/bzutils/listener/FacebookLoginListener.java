package com.bzutils.listener;

import java.util.EventListener;
import java.util.HashMap;

/**
 * Created by iaguila on 10/07/13.
 */
public interface FacebookLoginListener extends EventListener {

    public void onLoginSuccess(HashMap<String, String> user);

    public void onLoginFailed(String error);

}

package com.bzutils;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Message;

import com.bzutils.exception.BZLocationException;

import java.util.List;

/**
 * Created by iaguila on 10/07/13.
 */
public class Localizator {

    protected static final int TIMEOUT_MESSAGE = 0x100;
    public static final String KILOMETRES = "K";
    public static final String MILES = "M";
    LocationManager locationManager;
    //LocationListener locationListener;
    private Handler locationTimeoutHandler;
    private int locationTimeCount = 0;
    private int locationTimeout = 30;
    public boolean localizado = false;
    Context c;

    public Localizator(Context c){
        this.c=c;
    }

    public void localizate(final LocationListener listener) throws BZLocationException {

        LogBZ.getInstance().i("Getting user's location");

        locationManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        List<String> enabledProviders = locationManager.getProviders(true);

        if(enabledProviders.size() == 0 || (enabledProviders.size() == 1 && enabledProviders.get(0).equals("passive")) ) {
            listener.onProviderDisabled("ALL");
            LogBZ.getInstance().i("Location providers are disabled");
        }


        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 0, 0,
                    listener);
        }
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    listener);
        }

        locationTimeoutHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case TIMEOUT_MESSAGE:
                        locationManager.removeUpdates(listener);
                        listener.onProviderDisabled("TIMEOUT");
                        LogBZ.getInstance().e("Timeout getting location");

                        break;
                }
                super.handleMessage(msg);
            }
        };

        locationTimeoutHandler.postDelayed(locationTimer, 1000);
    }

    private Runnable locationTimer = new Runnable() {
        public void run() {
            locationTimeCount++;
            if (locationTimeCount < locationTimeout)
                locationTimeoutHandler.postDelayed(this, 1000);
            else {
                locationTimeoutHandler.removeCallbacks(this);
                Message msg = new Message();
                msg.what = TIMEOUT_MESSAGE;
                locationTimeoutHandler.sendMessage(msg);
            }
        }
    };

    public static double calculateDistance(Location from, Location to, String unit)
    {
        if(from == null)
            return -1;

        double lat1 = from.getLatitude();
        double lon1 = from.getLongitude();

        double lat2 = to.getLatitude();
        double lon2 = to.getLongitude();

        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344;
        } else if (unit == "M") {
            dist = dist * 0.8684;
        }
        return (dist);
    }


    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	    /*::  This function converts decimal degrees to radians             :*/
	    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg)
    {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	    /*::  This function converts radians to decimal degrees             :*/
	    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad)
    {
        return (rad * 180.0 / Math.PI);
    }



}


package com.bzutils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by iaguila on 10/07/13.
 */
public class AppRate {
    private static final String ALL_PREFS = ".a2r_ALL";
    private static final String INSTALL_DATE = ".a2r_INSTALL_DATE";
    private static final String HAS_RATED = ".a2r_HAS_RATED";
    private static final String APP_USES = ".a2r_APP_USES";

    public static void displayAsk2Rate(Context ctx, int days, int uses,
                                       boolean reminder, String message, String rateNow, String rateLater) {

        // Get Package Name
        String packageName = getPackageName(ctx);

        if (packageName == null)
            return; // no package, no way to find in market

        SharedPreferences sp = ctx.getSharedPreferences(
                packageName + ALL_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = ctx.getSharedPreferences(
                packageName + ALL_PREFS, Context.MODE_PRIVATE).edit();
        long installDate = sp.getLong(packageName + INSTALL_DATE, 0);
        if (installDate == 0) {
            editor.putLong(packageName + INSTALL_DATE,
                    System.currentTimeMillis());
            apply(editor);
        }

        boolean hasRated = sp.getBoolean(packageName + HAS_RATED, false);
        int appUses = sp.getInt(packageName + APP_USES, 0);

        if (appUses < uses) { // only ask if we have met use requirement
            return;
        } else if (!hasRated) {
            long currentTime = System.currentTimeMillis();
            if (((currentTime - installDate) / (1000 * 60 * 60 * 24)) >= days) {
                // SHOW
                createAsk2RateDialog(ctx, message, rateNow, rateLater);

                // don't let us show this dialog again
                if (!reminder) {
                    editor.putBoolean(packageName + HAS_RATED, true);
                    apply(editor);
                }
            }
        }
        return;
    }

    public static void displayAsk2RateWithCustomTitle(Context ctx, int days, int uses,
                                       boolean reminder, String message, String rateNow, String rateLater, String title) {

        // Get Package Name
        String packageName = getPackageName(ctx);

        if (packageName == null)
            return; // no package, no way to find in market

        SharedPreferences sp = ctx.getSharedPreferences(
                packageName + ALL_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = ctx.getSharedPreferences(
                packageName + ALL_PREFS, Context.MODE_PRIVATE).edit();
        long installDate = sp.getLong(packageName + INSTALL_DATE, 0);
        if (installDate == 0) {
            editor.putLong(packageName + INSTALL_DATE,
                    System.currentTimeMillis());
            apply(editor);
        }

        boolean hasRated = sp.getBoolean(packageName + HAS_RATED, false);
        int appUses = sp.getInt(packageName + APP_USES, 0);

        if (appUses < uses) { // only ask if we have met use requirement
            return;
        } else if (!hasRated) {
            long currentTime = System.currentTimeMillis();
            if (((currentTime - installDate) / (1000 * 60 * 60 * 24)) >= days) {
                // SHOW
                createAsk2RateDialogWithCustomTitle(ctx, message, rateNow, rateLater,title,7);

                // don't let us show this dialog again
                if (!reminder) {
                    editor.putBoolean(packageName + HAS_RATED, true);
                    apply(editor);
                }
            }
            else if (appUses==uses){
                createAsk2RateDialogWithCustomTitle(ctx, message, rateNow, rateLater,title,7);

                // don't let us show this dialog again
                if (!reminder) {
                    editor.putBoolean(packageName + HAS_RATED, true);
                    apply(editor);
                }
            }
        }
        return;
    }

    /**
     * increaseAppUsed()
     *
     * Use this method to increase the number uses. This is split from the
     * displayAsk2Rate call so you can have finer control over what constitutes
     * a session / use. For example if you only wanted to show the dialog after
     * a user made a purchase, call this in onPurchaseComplete() and not in
     * onCreate.
     *
     * @param ctx: Context from Activity, use getApplicationContext()
     */
    public static void increaseAppUsed(Context ctx) {
        String packageName = getPackageName(ctx);

        if (packageName == null)
            return; // no package, no way to find in market

        SharedPreferences sp = ctx.getSharedPreferences(
                packageName + ALL_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = ctx.getSharedPreferences(
                packageName + ALL_PREFS, Context.MODE_PRIVATE).edit();

        int uses = sp.getInt(packageName + APP_USES, 0);
        editor.putInt(packageName + APP_USES, ++uses);
        apply(editor);
    }

    private static String getPackageName(Context ctx) {
        String pkgName = null;
        try {
            PackageManager manager = ctx.getPackageManager();
            PackageInfo info = manager.getPackageInfo(ctx.getPackageName(), 0);
            pkgName = info.packageName;
        } catch (PackageManager.NameNotFoundException e) {
            // Can't find package name, set null
            e.printStackTrace();
        }
        return pkgName;
    }

    private static String getApplicationName(Context ctx) {
        String appName = "";
        try {
            PackageManager manager = ctx.getPackageManager();
            PackageInfo info = manager.getPackageInfo(ctx.getPackageName(), 0);
            appName = ctx.getResources().getString(
                    info.applicationInfo.labelRes);
        } catch (Exception e) {
            // Can't find AppName, leave blank
            e.printStackTrace();
        }
        return appName;
    }

    private static void createAsk2RateDialogWithCustomTitle(final Context ctx, String message, String rateNow, String rateLater, String title, int daysLater){
        AlertDialog.Builder marketDialog = new AlertDialog.Builder(ctx);
        final String packageName = getPackageName(ctx);
        final String appName = getApplicationName(ctx);

        final String marketLink = "market://details?id=" + packageName;
        marketDialog.setTitle(title);
        marketDialog.setMessage(message);
        marketDialog.setPositiveButton(rateNow,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Uri uri = Uri.parse(marketLink);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        ctx.startActivity(intent);
                        dialog.dismiss();

                        // Hide Dialog 4ever
                        SharedPreferences.Editor editor = ctx
                                .getSharedPreferences(packageName + ALL_PREFS,
                                        Context.MODE_PRIVATE).edit();
                        editor.putBoolean(packageName + HAS_RATED, true);
                        apply(editor);
                    }
                }).setNegativeButton(rateLater,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SharedPreferences.Editor editor = ctx
                                .getSharedPreferences(packageName + ALL_PREFS,
                                        Context.MODE_PRIVATE).edit();
                        editor.putLong(packageName + INSTALL_DATE, System.currentTimeMillis());
                        apply(editor);
                        dialog.cancel();
                    }
                });
        marketDialog.create().show();
    }

    private static void createAsk2RateDialog(final Context ctx, String message, String rateNow, String rateLater) {
        AlertDialog.Builder marketDialog = new AlertDialog.Builder(ctx);
        final String packageName = getPackageName(ctx);
        final String appName = getApplicationName(ctx);

        final String marketLink = "market://details?id=" + packageName;
        marketDialog.setTitle("Rate " + appName);
        marketDialog.setMessage(message);
        marketDialog.setPositiveButton(rateNow,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Uri uri = Uri.parse(marketLink);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        ctx.startActivity(intent);
                        dialog.dismiss();

                        // Hide Dialog 4ever
                        SharedPreferences.Editor editor = ctx
                                .getSharedPreferences(packageName + ALL_PREFS,
                                        Context.MODE_PRIVATE).edit();
                        editor.putBoolean(packageName + HAS_RATED, true);
                        apply(editor);
                    }
                }).setNegativeButton(rateLater,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        marketDialog.create().show();
    }

    /**
     * Muestra alerta en el momento que nosotros queramos.
     * @param ctx
     * @param message
     * @param rateNow
     * @param rateLater
     */
    public static void createAlertNow(final Context ctx, String message, String rateNow, String rateLater, String title, int dayslater){
        createAsk2RateDialogWithCustomTitle(ctx, message, rateNow, rateLater, title,dayslater);
    }

    // Faster pref saving for high performance
    private static final Method sApplyMethod = findApplyMethod();

    private static Method findApplyMethod() {
        try {
            Class<SharedPreferences.Editor> cls = SharedPreferences.Editor.class;
            return cls.getMethod("apply");
        } catch (NoSuchMethodException unused) {
            // fall through
        }
        return null;
    }

    public static void apply(SharedPreferences.Editor editor) {
        if (sApplyMethod != null) {
            try {
                sApplyMethod.invoke(editor);
                return;
            } catch (InvocationTargetException unused) {
                // fall through
            } catch (IllegalAccessException unused) {
                // fall through
            }
        }
        editor.commit();
    }
}


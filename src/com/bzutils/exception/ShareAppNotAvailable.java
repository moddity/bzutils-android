package com.bzutils.exception;

/**
 * Created by iaguila on 05/07/13.
 */
public class ShareAppNotAvailable extends Exception {

    public ShareAppNotAvailable(String st){
        super(st);
    }
}

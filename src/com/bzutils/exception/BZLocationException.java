package com.bzutils.exception;

/**
 * Created by iaguila on 10/07/13.
 */
public class BZLocationException extends Exception{

    public BZLocationException (String message){
        super(message);
    }
}

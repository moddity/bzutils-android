package com.bzutils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bzutils.exception.BZLocationException;
import com.bzutils.exception.ShareAppNotAvailable;
import com.bzutils.listener.ContactsListener;
import com.bzutils.model.BZPeriod;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by iaguila on 05/07/13.
 */
@SuppressLint("NewApi")
public class BZUtils {

    /*
                DECIMAL FORMATTER
     */
    public static final char DEFAULT_DECIMAL_SEPARATOR = '.';
    public static final int DEFAULT_DECIMAL_NUMBERS = 2;
    public static String decimalFunction(Double d, char separator, int numberDecimals, boolean adjustToMinimum){
        String decimals = DEFAULT_DECIMAL_SEPARATOR + "";
        for (int i = 0; i < numberDecimals; i++){
            decimals += "0";
        }
        String format = "###" + decimals;
        try{
            String temp = Double.toString(d);
            String retorn;
            DecimalFormat decimalFormat = new DecimalFormat(format);
            if(temp.startsWith("0"))retorn= "0"+decimalFormat.format(Double.parseDouble(temp)).replace(",", separator + "");
            else retorn= decimalFormat.format(Double.parseDouble(temp)).replace(",", separator + "");
            if(retorn.contains(decimals))retorn = retorn.replace(decimals, "");
            if (separator != DEFAULT_DECIMAL_SEPARATOR && retorn.contains(DEFAULT_DECIMAL_SEPARATOR + "")) retorn.replace(DEFAULT_DECIMAL_SEPARATOR, separator);
            if (adjustToMinimum) {
                while (retorn.endsWith("0") && retorn.contains(separator + "")) {
                    retorn = retorn.substring(0, retorn.length() - 1);
                }
            }
            return retorn;
        }catch(Exception eio){
            LogBZ.getInstance().printStackTrace(eio);
            return "0";
        }
    }
    public static String decimalFunction(String d, char separator, int numberDecimals, boolean adjustTuMinimum){
        return decimalFunction(Double.parseDouble(d), separator, numberDecimals, adjustTuMinimum);
    }
    public static String decimalFunction(Double d, char separator){
        return decimalFunction(d, separator, DEFAULT_DECIMAL_NUMBERS, true);
    }
    public static String decimalFunction(String d, char separator){
        return decimalFunction(d, separator, DEFAULT_DECIMAL_NUMBERS, true);
    }
    public static String decimalFunction(Double d, int numberDecimals){
        return decimalFunction(d, DEFAULT_DECIMAL_SEPARATOR, numberDecimals, true);
    }
    public static String decimalFunction(String d, int numberDecimals){
        return decimalFunction(d, DEFAULT_DECIMAL_SEPARATOR, numberDecimals, true);
    }
    public static String decimalFunction(Double d){
        return decimalFunction(d, DEFAULT_DECIMAL_SEPARATOR, DEFAULT_DECIMAL_NUMBERS, true);
    }
    public static String decimalFunction(String d){
        return decimalFunction(d, DEFAULT_DECIMAL_SEPARATOR, DEFAULT_DECIMAL_NUMBERS, true);
    }



    /*
                DATE FORMATTER
     */
    public static Date stringToDate(String date, String format) throws ParseException {
        DateFormat df = new SimpleDateFormat(format);
        return df.parse(date);
    }
    public static String dateToString(Date date, String format){
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }
    public static Date getDateFromTimeStamp(long timeStamp){
        return new Date(timeStamp * 1000);
    }
    public static String getDateFromTimeStamp(long timeStamp, String format){
        return dateToString(getDateFromTimeStamp(timeStamp), format);
    }



    /*
                TIME BETWEEN DATES
     */
    public static BZPeriod getTimeBetweenDates(Date d1, Date d2) {
         try{
            if(d1!=null && d2!=null){
                /*DateTime end = new DateTime(d2.getTime());
                DateTime start = new DateTime(d1.getTime());
                Period p = new Period(start, end);
                PeriodFormatter formatter;
                if(p.getDays()>0){
                    formatter = new PeriodFormatterBuilder()
                            .printZeroAlways().minimumPrintedDigits(1).appendDays().appendLiteral("d ")
                            .appendHours().appendLiteral("h ").appendMinutes()
                            .appendLiteral("m ").toFormatter();
                }else{
                    formatter = new PeriodFormatterBuilder()
                            .printZeroAlways().minimumPrintedDigits(1)
                            .appendHours().appendLiteral("h ").appendMinutes()
                            .appendLiteral("m ").toFormatter();
                }
                return p.toString(formatter);*/

                long milis = d1.getTime() - d2.getTime();
                if (milis < 0) milis = milis * (-1);
                Double min = (double) (milis / 1000 / 60);
                Double hour = min / 60;
                Double day = hour / 24;
                Double month = day / 30;
                Double year = month / 12;
                min = min - (hour.intValue() * 60);
                hour = hour - (day.intValue() * 24);
                day = day - (month.intValue() * 30);
                month = month - (year.intValue() * 12);
                BZPeriod period = new BZPeriod();
                period.setYears(year.intValue());
                period.setMonths(month.intValue());
                period.setDays(day.intValue());
                period.setHours(hour.intValue());
                period.setMinutes(min.intValue());
                return period;
            }
        }catch(Exception eio){
            LogBZ.getInstance().printStackTrace(eio);
        }
        return null;
    }


    /*
                ALERT METHODS
     */
                                /*
                                            SIMPLE MESSAGE
                                 */
    public static void showSimpleMessage(final Activity ctx, final String text, final String buttonText, final String title, final Integer icon, final boolean closeActivity, final DialogInterface.OnClickListener clickListener){
        ctx.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
                dialog.setMessage(text);
                if (title != null) dialog.setTitle(title);
                if (icon != null) dialog.setIcon(icon);
                dialog.setCancelable(false);
                dialog.setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        if (clickListener != null)
                            clickListener.onClick(dialog, which);
                        dialog.dismiss();
                        if (closeActivity)
                            ctx.finish();
                    }
                });
                AlertDialog alert = dialog.create();
                alert.show();
            }
        });

    }

    public static void showSimpleMessageWithTwoButtons(final Activity ctx, final String text, final String texte1, final String texte2, final DialogInterface.OnClickListener escolta1, final DialogInterface.OnClickListener escolta2){

        ctx.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
                dialog.setMessage(text);
                dialog.setCancelable(false);
                dialog.setPositiveButton(texte1,escolta1);
                dialog.setNegativeButton(texte2, escolta2);
                AlertDialog alert = dialog.create();
                alert.show();
            }
        });

    }


    public static void showSimpleMessage(Activity ctx, String text, String buttonText, String title, boolean closeActivity){
        showSimpleMessage(ctx, text, buttonText, title, null, closeActivity, null);
    }
    public static void showSimpleMessage(Activity ctx, String text, String buttonText, String title){
        showSimpleMessage(ctx, text, buttonText, title, null, false, null);
    }
    public static void showSimpleMessage(Activity ctx, String text, String buttonText, Integer icon, boolean closeActivity){
        showSimpleMessage(ctx, text, buttonText, null, icon, closeActivity, null);
    }
    public static void showSimpleMessage(Activity ctx, String text, String buttonText, Integer icon){
        showSimpleMessage(ctx, text, buttonText, null, icon, false, null);
    }
    public static void showSimpleMessage(Activity ctx, String text, String buttonText, boolean closeActivity){
        showSimpleMessage(ctx, text, buttonText, null, null, closeActivity, null);
    }
    public static void showSimpleMessage(Activity ctx, String text, String buttonText){
        showSimpleMessage(ctx, text, buttonText, null, null, false, null);
    }
    public static void showSimpleMessage(Activity ctx, String text, boolean closeActivity){
        showSimpleMessage(ctx, text, "OK", closeActivity);
    }
    public static void showSimpleMessage(Activity ctx, String text, DialogInterface.OnClickListener onClickListener){
        showSimpleMessage(ctx, text, "OK", null, null, false, onClickListener);
    }
    public static void showSimpleMessage(Activity ctx, String text){
        showSimpleMessage(ctx, text, "OK", false);
    }

                                   /*
                                            RETRY MESSAGE
                                    */
    public static void showRetryError(Context ctx, String text, DialogInterface.OnClickListener clickListener, String cancelButton, String retryButton, String title, Integer icon){
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        dialog.setMessage(text);
        if (title != null) dialog.setTitle(title);
        if (icon != null) dialog.setIcon(icon);
        dialog.setCancelable(false);
        dialog.setNegativeButton(cancelButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.setPositiveButton(retryButton, clickListener);
        AlertDialog alert = dialog.create();
        alert.show();
    }
    public static void showRetryError(Context ctx, String text, DialogInterface.OnClickListener clickListener, String cancelButton, String retryButton, String title){
        showRetryError(ctx, text, clickListener, cancelButton, retryButton, title, null);
    }
    public static void showRetryError(Context ctx, String text, DialogInterface.OnClickListener clickListener, String cancelButton, String retryButton){
        showRetryError(ctx, text, clickListener, cancelButton, retryButton, null, null);
    }
    public static void showRetryError(Context ctx, String text, DialogInterface.OnClickListener clickListener){
        showRetryError(ctx, text, clickListener, "OK", "Reintentar");
    }

                            /*
                                    CUSTOM ALERT
                             */
    public static void showCustomAlert(Context ctx, View v, String message, int textViewId, Button button1, View.OnClickListener listener1){
        showCustomAlert(ctx, v, message, textViewId, button1, listener1, null, null);
    }


    public static void showCustomAlert(Context ctx, View v, String message, int textViewId, Button button1, View.OnClickListener listener1, Button button2, View.OnClickListener listener2){
        showCustomAlert(ctx, v, message, textViewId, button1, listener1, button2, listener2, null, null);
    }

    public static void showCustomAlert(Context ctx, View v, String message, int textViewId, Button button1, final View.OnClickListener listener1, Button button2, final View.OnClickListener listener2,
                                       Button button3, final View.OnClickListener listener3){
        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (message != null && textViewId != 0){
            TextView msg = (TextView) v.findViewById(textViewId);
            msg.setText(message);
        }
        dialog.setContentView(v);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener1 != null)
                    listener1.onClick(v);
                dialog.dismiss();
            }
        });
        if (button2 != null)
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener2 != null)
                        listener2.onClick(v);
                    dialog.dismiss();
                }
            });
        if (button3 != null)
            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener3 != null)
                        listener3.onClick(v);
                    dialog.dismiss();
                }
            });

        dialog.show();
    }

    /*
                SHARE METHODS
     */

                                /*
                                            SHARE MAIL
                                 */
    public static void shareViaMail(Context ctx, String mailBody, String subject, String destine, String[] destines, Uri attachedFile){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.setPackage("com.google.android.gm");
        if (destine != null) {
            String [] st = {destine};
            i.putExtra(Intent.EXTRA_EMAIL, st);
        }
        if (destines != null){
            i.putExtra(Intent.EXTRA_BCC, destines);
        }
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT, mailBody);
        if (attachedFile != null) i.putExtra(Intent.EXTRA_STREAM, attachedFile);

        ctx.startActivity(i);
    }
    public static void shareViaMail(Context ctx, String mailBody, String subject, String destine, String[] destines){
        shareViaMail(ctx, mailBody, subject, destine, destines, null);
    }
    public static void shareViaMail(Context ctx, String mailBody, String subject, String destine, Uri attachedFile){
        shareViaMail(ctx, mailBody, subject, destine, null, attachedFile);
    }
    public static void shareViaMail(Context ctx, String mailBody, String subject, String[] destines, Uri attachedFile){
        shareViaMail(ctx, mailBody, subject, null, destines, attachedFile);
    }
    public static void shareViaMail(Context ctx, String mailBody, String subject, String destine){
        shareViaMail(ctx, mailBody, subject, destine, null, null);
    }
    public static void shareViaMail(Context ctx, String mailBody, String subject, String destines[]){
        shareViaMail(ctx, mailBody, subject, null, destines, null);
    }
    public static void shareViaMail(Context ctx, String mailBody, String subject, Uri attachedFile){
        shareViaMail(ctx, mailBody, subject, null, null, attachedFile);
    }
    public static void shareViaMail(Context ctx, String mailBody, String subject){
        shareViaMail(ctx, mailBody, subject, null, null, null);
    }

                                /*
                                            SHARE FROM OTHER APP
                                 */
    public static final String WHATSAPP_PACKAGE = "com.whatsapp";
    public static final String WHATSAPP_CLASSNAME = "com.whatsapp.ContactPicker";
    public static final String TWITTER_PACKAGE = "com.twitter.android";
    public static final String TWITTER_CLASSNAME = "com.twitter.android.PostActivity";
    public static void shareViaApp(Context ctx, String appPackage, String appClassName, String shareBody) throws ShareAppNotAvailable {
        if (isAppInstalled(ctx, appPackage)){
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/html");
            intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            intent.setClassName(appPackage, appClassName);
            try{
                ctx.startActivity(intent);
            } catch (Exception e){
                throw new ShareAppNotAvailable(appPackage + " not available");
            }
        }
        else{
            throw new ShareAppNotAvailable(appPackage + " not installed");
        }
    }
    public static boolean isAppInstalled(Context ctx, String appPackage){
        final Intent shareIntent = new Intent(
                android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        PackageManager pm = ctx.getPackageManager();
        List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent,
                0);
        for (final ResolveInfo app : activityList) {
            final ActivityInfo ai = app.activityInfo;
            String pkg = ai.packageName;
            if (pkg.contains(appPackage)) {
                return true;
            }
        }
        return false;
    }


    /*
                    DNI VALIDATOR
     */
    @SuppressLint("DefaultLocale")
    public static boolean DNIvalidation(String nif){
        LogBZ.getInstance().v("Validando dni - " + nif);
        if (nif == null)
            return false;
        nif = nif.replace(" ", "");
        nif = nif.replace("-", "");
        if (nif.toUpperCase().startsWith("X") || nif.toUpperCase().startsWith("Y") || nif.toUpperCase().startsWith("Z"))
            return false;

        nif = nif.toUpperCase();
        int temp = 0;
        if (nif.toUpperCase().startsWith("X"))
            temp = 0;
        if (nif.toUpperCase().startsWith("Y"))
            temp = 1;
        if (nif.toUpperCase().startsWith("Z"))
            temp = 2;

            // si es NIE, eliminar la x,y,z inicial para tratarlo como nif
        if (nif.toUpperCase().startsWith("X") || nif.toUpperCase().startsWith("Y") || nif.toUpperCase().startsWith("Z"))
            nif = nif.substring(1);

        Pattern nifPattern = Pattern.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKE])");
        Matcher m = nifPattern.matcher(nif);
        if (m.matches()) {
            String letra = m.group(2);
            String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
            int dni = Integer.parseInt(Integer.toString(temp) + m.group(1));
            dni = dni % 23;
            String reference = letras.substring(dni, dni + 1);

            if (reference.equalsIgnoreCase(letra)) {
                return true;
            } else {
                return false;
            }
        } else
        return false;
    }

    public static boolean NIEvalidation(String nie){
        return DNIvalidation(nie);
    }


    /*
                    RATE MY APP
     */
    public static void configRateMyApp(Context ctx, int days, int uses,
                                       boolean reminder, String message, String rateNow, String rateLater){

        AppRate.displayAsk2Rate(ctx, days, uses, reminder, message, rateNow, rateLater);

    }

    public static void configRateMyAppWithCustomTitle(Context ctx, int days, int uses,
                                       boolean reminder, String message, String rateNow, String rateLater, String title){

        AppRate.displayAsk2RateWithCustomTitle(ctx, days, uses, reminder, message, rateNow, rateLater, title);

    }

    public static void increasesAppUsedToRate(Context ctx){
        AppRate.increaseAppUsed(ctx);
    }


    /*
                    LOCATION METHODS
     */
                /*
                                GET MY GEOLOCATION
                 */
    public static void myGeoLocation(Context context, LocationListener listener) throws BZLocationException {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) &&  !manager.isProviderEnabled( LocationManager.NETWORK_PROVIDER )) {
            throw new BZLocationException("Location provider not enabled");
        }

        if (manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
            listener.onLocationChanged(manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
        } else if (manager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
            listener.onLocationChanged(manager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
        }


        Localizator localizator = new Localizator(context);
        localizator.localizate(listener);
    }

                /*
                                CALCULATE DINSTANCE
                 */
    public static double calculateLocationsDistance(Context ctx, Location from, Location to, String unit){
        return Localizator.calculateDistance(from, to, unit);
    }
    public static double calculateLocationsDistance(Context ctx, double fromLat, double fromLong, double toLat, double toLong, String unit){
        //Creamos las dos Locations para calcular la distancia
        Location from = new Location("");
        from.setLongitude(fromLong);
        from.setLatitude(fromLat);
        Location to = new Location("");
        to.setLongitude(toLong);
        to.setLatitude(toLat);
        return calculateLocationsDistance(ctx, from, to, unit);
    }
    public static double calculateLocationsDistance(Context ctx, Location from, Location to){
        return calculateLocationsDistance(ctx, from, to, Localizator.KILOMETRES);
    }
    public static double calculateLocationsDistance(Context ctx, double fromLat, double fromLong, double toLat, double toLong){
        return calculateLocationsDistance(ctx, fromLat, fromLong, toLat, toLong, Localizator.KILOMETRES);
    }


    /*
                QR GENERATOR
     */
    public static String generateQR(String code, int size){
        return "http://api.qrserver.com/v1/create-qr-code/?size=" + size + "x" + size + "&data=" + code;
    }
    public static String generateQR(String code){
        return generateQR(code, 100);
    }


    /*
                CAPITALIZE FIRST LETTER
     */
    public static String capitalizeFirstLetter(String text){
        if (text == null) return null;
        if (text.length() == 0) return "";
        return text.substring(0,1).toUpperCase() + text.substring(1);
    }

    /*
                GET PHONEBOOK CONTACTS
     */
    public static void getContactsFromPhoneBook(Activity activity, ContactsListener listener){
        PhoneBookMethods.getPhonebookContacts(activity, listener);
    }


    /*
                FORMALIZE STRING -treu els accents-
     */
    @SuppressLint("NewApi")
	public static String formalizeString(String key, Locale locale){
        key = Normalizer.normalize(key, Normalizer.Form.NFD);
        return key.replaceAll("[^\\p{ASCII}]", "");
    }


    /*
                SCREEN SIZE
     */
    @SuppressLint("NewApi")
	public static int getScreenWidht(Activity ctx){
        Display display = ctx.getWindowManager().getDefaultDisplay();
        if (Build.VERSION.SDK_INT < 13){
            return display.getWidth();
        }
        else{
            Point p = new Point();
            display.getSize(p);
            return p.x;
        }
    }

    @SuppressLint("NewApi")
	public static int getScreenHeight(Activity ctx){
        Display display = ctx.getWindowManager().getDefaultDisplay();
        if (Build.VERSION.SDK_INT < 13){
            return display.getHeight();
        }
        else{
            Point p = new Point();
            display.getSize(p);
            return p.y;
        }
    }


                    /*
                            EMAIL VALIDATOR
                     */

    public static boolean checkEmail(String email){
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE
    );


                    /*
                            HIDE/SHOW KEYBOARD
                     */
    public static void showKeyboard(Context ctx, EditText editText){
        InputMethodManager mgr = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        editText.requestFocus();
    }
    public static void hideKeyboard(Activity ctx){
        InputMethodManager mgr = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(ctx.getWindow().getDecorView().getWindowToken(), 0);
    }

    public static String md5(String string){
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(string.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            LogBZ.printStackTrace(e);
        }
        return "";
    }

    public static void printSignature(Context context){
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                LogBZ.d("KeyHash: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            LogBZ.printStackTrace(e);
        } catch (NoSuchAlgorithmException e) {
            LogBZ.printStackTrace(e);
        }
    }
}

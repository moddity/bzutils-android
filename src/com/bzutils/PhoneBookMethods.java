package com.bzutils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import com.bzutils.listener.ContactsListener;
import com.bzutils.model.Contact;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by iaguila on 19/07/13.
 */
class PhoneBookMethods {

    private static ArrayList<String> correos;
    private static ContactsListener contactsListener;

    public static void getPhonebookContacts(Activity act, ContactsListener listener){
        contactsListener = listener;
        new getContacts().execute(act);
    }
    private static ArrayList<Contact> readContacts(Activity activity){
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        Integer x=0;
        Cursor phones = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        while (phones.moveToNext())
        {
            Contact contacto = new Contact();
            try{
                String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Nickname.DISPLAY_NAME));
                String phone = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                contacto.setName(name);
                contacto.setPhone(phone);
                contacto.setId(x.toString());
                contacto.setEmails(getEmail(activity, name));
                Drawable foto = new BitmapDrawable(loadContactPhoto(getId(activity, contacto.getName()),activity));
                foto = resizeDrawable(foto);
                contacto.setFoto(foto);
                contacts.add(contacto);
            }catch(Exception eio){
                //.println("no te mail.");
                LogBZ.getInstance().printStackTrace(eio);
            }

            x++;

        }
        phones.close();
        Collections.sort(contacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact lhs, Contact rhs) {
                // TODO Auto-generated method stub
                return lhs.getName().compareToIgnoreCase(rhs.getName());
            }
        });

        ArrayList<Contact> contactos = new ArrayList<Contact>();
        ArrayList<Contact> temp = contacts;
        for (int i = 0; i < contacts.size(); i ++){
            for (int j = 0; j < temp.size(); j++){
                if (temp.get(j).getEmails()!=null && temp.get(j).getName().equals(contacts.get(i).getName())){
                    for (int k = 0; k < temp.get(j).getEmails().size(); k ++){
                        if (!contacts.get(i).getEmails().contains(temp.get(j).getEmails().get(k)))
                            contacts.get(i).getEmails().add(temp.get(j).getEmails().get(k));
                    }
                    String name = temp.get(j).getName();
                    name = name.toUpperCase();
                    break;
                }
            }
            if (!contactos.contains(contacts.get(i)))
                contactos.add(contacts.get(i));
        }

        return contactos;
    }

    private static ArrayList<String> getEmail(Activity act, String NAME){
        ContentResolver cr = act.getContentResolver();
        String email = "";
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                "DISPLAY_NAME = '" + NAME + "'", null, null);
        act.startManagingCursor(cursor);
        if (cursor.moveToFirst()) {
            String contactId =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

            Cursor emails = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId, null, null);
            act.startManagingCursor(emails);
            String name=NAME;

            String anterior="";
            while (emails.moveToNext()) {

                email = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));


                if(!anterior.equalsIgnoreCase(emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)))){
                    correos.add(email);
                }

                anterior=email;
            }
            emails.close();
        }
        cursor.close();

        return correos;
    }

    public static Bitmap loadContactPhoto(long id,Context c) {
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(c.getContentResolver(), uri);
        if (input == null) {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }

    private static long getId(Activity act, String NAME){
        Cursor cursor = act.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null,
                "DISPLAY_NAME = '" + NAME + "'", null, null);
        if (cursor.moveToFirst()) {
            String contactId =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            return Long.parseLong(contactId);
        }
        cursor.close();
        return 0;
    }

    private static Drawable resizeDrawable(Drawable d){

        if (d != null) {
            Bitmap bitmapOrg = ((BitmapDrawable) d).getBitmap();
            if (bitmapOrg != null){
                int width = bitmapOrg.getWidth();
                int height = bitmapOrg.getHeight();
                int newWidth = 50;
                int newHeight = 50;
                // calculate the scale
                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;
                // create a matrix for the manipulation
                Matrix matrix = new Matrix();
                // resize the bit map
                matrix.postScale(scaleWidth, scaleHeight);
                Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0,
                        width, height, matrix, true);
                // make a Drawable from Bitmap to allow to set the BitMap
                d = new BitmapDrawable(resizedBitmap);
            }
        }

        return d;
    }

    private static class getContacts extends AsyncTask<Activity, Void, ArrayList<Contact>>{
        @Override
        protected ArrayList<Contact> doInBackground(Activity... arg0) {
            return readContacts(arg0[0]);
        }
        @Override
        protected void onPostExecute(ArrayList<Contact> res){
            if (res == null){
                if (contactsListener != null)
                    contactsListener.onContactsFailed("Error getting phonebook contacts");
            }
            else if (contactsListener != null)
                contactsListener.onContactsReceived(res);

        }
    }
}

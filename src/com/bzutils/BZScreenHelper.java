package com.bzutils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by Bazinga Systems on 3/12/13.
 */
public class BZScreenHelper {

    public static int width;
    public static int height;

    /**
     * Funcion que transforma dps a pixels.
     * @param dp
     * @return
     */
    public static int dpToPx(Context ctx, int dp) {
        return (int) (dp * ctx.getResources().getDisplayMetrics().density + 0.5f);
    }
    /**
     * Funcion que redimensiona imagenes para que se ajusten al alto y ancho que nos envian.
     * @param imageView
     */
    public static void resizeImageView(Context ctx, ImageView imageView, RelativeLayout layoutselected){
        getScreenMeasures(ctx);
        imageView.setMaxWidth(width/4);
        imageView.setMaxHeight(width/4);
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) imageView.getLayoutParams();
        params.width = width/4;
        params.height = width/4;
        imageView.setLayoutParams(params);
        if(layoutselected!=null)layoutselected.setLayoutParams(params);
    }

    /**
     * Obtiene y setea medidas de la pantalla
     * @param ctx
     */
    public static void getScreenMeasures(Context ctx){
        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        width = metrics.widthPixels;
        height = metrics.heightPixels;
    }

    public static int getScreenWidth(Context ctx) {
        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        return metrics.widthPixels;
    }

    public static int getScreenHeight(Context ctx) {
        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        return metrics.heightPixels;
    }

    /**
     * Obtiene dp si le pasas pixels.
     * @param px
     * @param context
     * @return
     */
    public static float dpFromPx(float px, Context context)
    {
        return px / context.getResources().getDisplayMetrics().density;
    }

    /**
     * Obtiene la vista principal del Activity.
     * @param currentActivity
     * @return
     */
    public static View getViewFromActivity(Activity currentActivity){

        return currentActivity.getWindow().getDecorView().findViewById(android.R.id.content);
    }

    public static void checkHeightDifference(View v, int mLastHeightDifferece){
        // get screen frame rectangle
        Rect r = new Rect();

        v.getWindowVisibleDisplayFrame(r);
        // get screen height
        int screenHeight = v.getRootView().getHeight();
        // calculate the height difference
        int heightDifference = screenHeight - (r.bottom - r.top);

        // if height difference is different then the last height difference and
        // is bigger then a third of the screen we can assume the keyboard is open
        if (heightDifference > screenHeight/3 && heightDifference != mLastHeightDifferece) {
            // keyboard visiblevisible
            // get root view layout params
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) v.getLayoutParams();
            // set the root view height to screen height minus the height difference
            lp.height = screenHeight - heightDifference;
            // call request layout so the changes will take affect
            v.requestLayout();
            // save the height difference so we will run this code only when a change occurs.
            mLastHeightDifferece = heightDifference;
        } else if (heightDifference != mLastHeightDifferece) {
            // keyboard hidden
            // get root view layout params and reset all the changes we have made when the keyboard opened.
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) v.getLayoutParams();
            lp.height = screenHeight;
            // call request layout so the changes will take affect
            v.requestLayout();
            // save the height difference so we will run this code only when a change occurs.
            mLastHeightDifferece = heightDifference;
        }
    }
}

package com.bzutils;

public class LogBZ {

    private static LogBZ instance = null;

    private Boolean logOnTestFlight = new Boolean(false);

    public static LogBZ getInstance(){
        if (instance == null)
            instance = new LogBZ();

        return instance;
    }

    private String TAG = "LogBZ";
    private boolean DEBUG = true;

    public void i(String string) {
        if(DEBUG){
            android.util.Log.i(TAG, string);
        }
    }
    public static void e(String string) {
        if(getInstance().DEBUG){
            android.util.Log.e(getInstance().TAG, string);

        }
    }
    public static void d(String string) {
        if(getInstance().DEBUG){
            android.util.Log.d(getInstance().TAG, string);
        }
    }
    public static void v(String string) {
        if(getInstance().DEBUG){
            android.util.Log.v(getInstance().TAG, string);
        }
    }
    public static void w(String string) {
        if(getInstance().DEBUG){
            android.util.Log.w(getInstance().TAG, string);
        }
    }
    public static void printStackTrace(Exception e){
        if (getInstance().DEBUG){
            e.printStackTrace();
        }
    }


    public static void setTAG(String TAG) {
        getInstance().TAG = TAG;
    }
    public static void setDEBUG(boolean debug){
        getInstance().DEBUG = debug;
    }

    public Boolean getLogOnTestFlight() {
        return logOnTestFlight;
    }

    public void setLogOnTestFlight(Boolean logOnTestFlight) {
        this.logOnTestFlight = logOnTestFlight;
    }
}
